#!/bin/sh
set -eux

# to start we create just 1GB sparse filesystem and add 
# more space if it is about to fill up
if [ ! -e /var-lib-docker.loopback.ext4 ]; then
  dd of=/var-lib-docker.loopback.ext4 bs=1 seek=1G count=0
  /sbin/mkfs.ext4 -q /var-lib-docker.loopback.ext4
fi

# and mount file system to data-root-dir
mount -t ext4 -o loop /var-lib-docker.loopback.ext4 /var-lib-docker

/usr/local/bin/dockerd-entrypoint.sh "$@"
